import json
import asyncore
import logging
import socket
import sys
from select import select
class TcpClient(asyncore.dispatcher):
    """Sends messages to the server and receives responses.
    """

    def __init__(self, host, port, message, chunk_size=512):
        self.message = message
        self.to_send = message
        self.received_data = []
        self.chunk_size = chunk_size
        self.logger = logging.getLogger('Client')
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.debug('connecting to %s', (host, port))
        self.connect((host, port))
        return

    def handle_connect(self):
        self.logger.debug('Connected......')

    def handle_close(self):
        self.logger.debug('closing the connection')
        self.close()
        received_message = ''.join(self.received_data)
        self.logger.debug(received_message)

        return

    def writable(self):
        timeout = 10
        rlist, _, _ = select([sys.stdin], [], [], timeout)
        if rlist :
            self.to_send = sys.stdin.readline()
            return True
        else :
            return False


    def handle_write(self):
        sent = self.send(self.to_send[:self.chunk_size])
        self.to_send = self.to_send[sent:]

    def handle_read(self):
        data = self.recv(self.chunk_size)
        self.logger.debug('recieved -> "%s"',  data)



if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG,
                        format='%(name)s: %(message)s',
                        )

    configparam = json.load(open("connection.json"))
    server = configparam["CLIENT"]
    HOST = str(server["IP"])
    PORT = int(server["port"])

    client = TcpClient(HOST, PORT, message="Start")

    asyncore.loop()