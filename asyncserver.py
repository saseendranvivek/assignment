import asyncore
import logging
import socket
import json

class TransactionServer (asyncore.dispatcher):
    mychannels = set()  # to keeptrack of channels
    maxchannels = 10
    def __init__(self , address):

        self.logger = logging.getLogger(__name__)
        logfilename = 'server_' + address[0] + '_' + str(address[1]) + '.log'
        fh = logging.FileHandler('%s' % (logfilename))
        self.logger.setLevel(logging.INFO)
        formater = logging.Formatter('%(asctime)s %(message)s',
                                     datefmt='%m/%d/%Y %I:%M:%S %p')
        fh.setFormatter(formater)
        self.logger.addHandler(fh)
        asyncore.dispatcher.__init__(self)

        try :
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind(address)
        except socket.error as msg:
            self.logger.error("Socket binding error: " + str(msg))
            self.close()
            return

        self.address = self.socket.getsockname()
        self.connection_count = 0
        self.logger.info('Server successfully initialized : %s \n '
                         'Lets wait for connections......', self.address)
        self.listen(5)


    @classmethod
    def getchannel(cls):
        activechannels = len(cls.mychannels)
        if activechannels >= cls.maxchannels :
            raise ValueError ("Maximum channel reached")
        else :
            for i in range(1, cls.maxchannels+1):
                if i not in cls.mychannels :
                    cls.mychannels.add(i)
                    return i

    @classmethod
    def releasechannel(cls, channelno):

        if channelno in cls.mychannels :
            cls.mychannels.remove(channelno)
            cls.maxchannels -= 1


    def handle_accept(self):
        # Called when a client connects to our socket
        client_info = self.accept()
        if client_info is not None:
            try :
                channelno = TransactionServer.getchannel()

            except ValueError:
                self.logger.info("Maximum connection reached...")
                return

            self.logger.info('New connection established -> %s Channel : %d', client_info[1], channelno)
            ClientHandler(client_info[0], client_info[1], channelno, self.logger)


class ClientHandler(asyncore.dispatcher):
    def __init__(self, sock, address,connection, logger):
        asyncore.dispatcher.__init__(self, sock)
        self.channelno = connection
        self.logger = logger
        self.data_to_write = list()
        self.readstring = ''
        self.emptycounter = -1;
        self.maxcounter = 5;

    #https: // docs.python.org / 2 / library / asyncore.html
    #Called each time around the asynchronous loop to determine
    #The default method simply returns True, indicating that by default, all channels will be interested in write events.
    def writable(self):
        self.logger.info("Channel : %d , Called writeable  return value is %d", self.channelno, bool(self.data_to_write))

        return bool(self.data_to_write)

    # https: // docs.python.org / 2 / library / asyncore.html
    #Called when the asynchronous loop detects that a writable socket can be written.
    # Often this method will implement the necessary buffering for performance.
    def handle_write(self):
        data = self.data_to_write.pop()
        sent = self.send(data[:1024])
        if sent < len(data):
            remaining = data[sent:]
            self.data.to_write.append(remaining)
        self.logger.info('Channel : %d, Called handled_write. sent (%d) bytes', self.channelno, sent)


    #https://docs.python.org/2/library/asyncore.html

    def handle_read(self):
        data = self.recv(1024)
        self.logger.info('Channel : %d, Called handle_read. red (%d)', self.channelno, len(data))
        # insert the back of the list so that it will act as a queue.
        while '\n' in data :
            newdta = data.split('\n',1)
            writeline = self.readstring.join(newdta[0])
            self.data_to_write.insert(0,writeline[::-1] + '\n')
            self.readstring = ''
            data = newdta[1]
        else:
            self.readstring = self.readstring.join(data)

    #Called when the socket is closed.
    def handle_close(self):
        self.logger.info('Channel : %d, Called handle_close ', self.channelno)
        TransactionServer.releasechannel(self.channelno)
        self.close()


def main():
#    logging.basicConfig(level=logging.DEBUG, format='%(name)s:[%(levelname)s]: %(message)s')
#    HOST = '127.0.0.1'
#    PORT = 1507
    configparam = json.load(open("connection.json"))
    server = configparam["SERVER"]
    HOST = str(server["IP"])
    PORT = int(server["port"])
    TransactionServer.maxconnection = int(server["maxconnection"])
    s = TransactionServer((HOST,PORT))
    asyncore.loop()


if __name__ == '__main__':
     main()

