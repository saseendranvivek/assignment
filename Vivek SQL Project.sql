
--DDLs
-- Student table
CREATE TABLE sosr01.Student
(
   Student_id   NUMBER (5) PRIMARY KEY,
   Name         VARCHAR2 (100) NOT NULL,
   Age          NUMBER (10)
);


--Course table
CREATE TABLE sosr01.Course
(
   Course_id     NUMBER (5) PRIMARY KEY,
   Course_name   VARCHAR2 (100) NOT NULL
);


--Student_course_Relation table
CREATE TABLE sosr01.Stu_Cou_Relation
(
   Stu_Cou_ID   NUMBER (5) PRIMARY KEY,
   Student_id   NUMBER (5),
   Course_id    NUMBER (5)
);

--Create Unique index for Composite key
CREATE UNIQUE INDEX SOSR01.UK_STU_COU_ID
   ON SOSR01.Stu_Cou_Relation (Student_id, Course_id);

--Foreign Key
ALTER TABLE SOSR01.Stu_Cou_Relation ADD (
  CONSTRAINT FK_STU_ID
  FOREIGN KEY (Student_id)
  REFERENCES SOSR01.STUDENT (STUDENT_ID),
  FOREIGN KEY (COURSE_ID)
  REFERENCES SOSR01.COURSE (COURSE_ID));

--DMLs 
--Insert data for Student table
Insert into SOSR01.STUDENT
   (STUDENT_ID, NAME, AGE)
 Values
   (5, 'Mark', 34);
Insert into SOSR01.STUDENT
   (STUDENT_ID, NAME, AGE)
 Values
   (4, 'Arjun', 25);
Insert into SOSR01.STUDENT
   (STUDENT_ID, NAME, AGE)
 Values
   (3, 'Tom', 29);
Insert into SOSR01.STUDENT
   (STUDENT_ID, NAME, AGE)
 Values
   (2, 'Ram', 31);
Insert into SOSR01.STUDENT
   (STUDENT_ID, NAME, AGE)
 Values
   (1, 'Vivek', 30);
COMMIT;


--Insert data for Course TABLE
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (1, 'CS101');
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (2, 'CS102');
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (3, 'CS103');
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (4, 'CS104A');
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (5, 'CS104B');
Insert into SOSR01.COURSE
   (COURSE_ID, COURSE_NAME)
 Values
   (6, 'CS105');
COMMIT;


--Insert data for Student_Course_Relation table
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (10, 5, 4);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (9, 4, 4);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (8, 4, 2);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (7, 4, 1);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (6, 3, 1);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (5, 2, 5);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (4, 2, 1);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (3, 1, 4);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (2, 1, 2);
Insert into SOSR01.STU_COU_RELATION
   (STU_COU_ID, STUDENT_ID, COURSE_ID)
 Values
   (1, 1, 1);
COMMIT;



--Find all students who have taken course_id=CS101
SELECT student.name, course.course_name
  FROM student, course, stu_cou_relation
 WHERE     stu_cou_relation.STUDENT_ID = student.STUDENT_ID
       AND stu_cou_relation.course_id = course.course_id
       AND course.course_name = 'CS101';
